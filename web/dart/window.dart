import 'dart:html';

import 'package:chrome/chrome_app.dart';
import 'package:polymer/polymer.dart';
import 'package:byte_serve_app/src/downloads_view/downloads_view.dart';

import 'message_to_background.dart';
import 'message_to_window.dart';

DownloadsView get mainView => querySelector('#main-view') as DownloadsView;

void main() {
  initPolymer();
  
  window.addEventListener('polymer-ready', (event) {
    runtime.sendMessage(new MessageToBackground(windowIsNowReady: true));
  });
  
  runtime.onMessage.listen((OnMessageEvent event) {
    if (event.message is MessageToWindow) {
      var message = (event.message as MessageToWindow);
      
      if (message.newUrl != null) {        
        mainView.addNewDownload(message.newUrl);
      }
    }
  });
}
