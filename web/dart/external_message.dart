library byte_serve.external_message;

class ExternalMessage {
  String newUrl;
  
  ExternalMessage({String newUrl}) {
    this.newUrl = newUrl;
  }
}
