library byte_serve.message_to_background;

class MessageToBackground {
  bool windowIsNowReady = false;
  
  MessageToBackground({bool windowIsNowReady}) {
    this.windowIsNowReady = windowIsNowReady;
  }
}
