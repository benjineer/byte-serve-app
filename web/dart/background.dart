import 'dart:async';
import 'package:chrome/chrome_app.dart';

import 'external_message.dart';
import 'message_to_background.dart';
import 'message_to_window.dart';

const String _windowUrl = 'window.html';

AppWindow _window;
bool _windowIsReady = false;
List<String> _queuedUrls = [];

/// Registers listeners
void main() {  
  app.runtime.onLaunched.listen((launchData) {
    createWindow();
  });

  // handle messages from  other extensions
  runtime.onMessageExternal.listen((OnMessageExternalEvent event) {      
    if (event.message is ExternalMessage) {
      var message = (event.message as ExternalMessage);
  
      createWindow().then((x) {    
        
        if (message.newUrl != null) {          
          if (_windowIsReady) {
            runtime.sendMessage(new MessageToWindow(newUrl: message.newUrl));
          }          
          else {
            _queuedUrls.add(message.newUrl);
          }
        }
      });
    }
  });
  
  // handle messages from open windows
  runtime.onMessage.listen((OnMessageEvent event) {
    if (event.message is MessageToBackground) {
      var message = (event.message as MessageToBackground);
      
      if (message.windowIsNowReady) {
        _windowIsReady = true;
        
        var url;
        do {
          url = _queuedUrls.removeAt(0);
          runtime.sendMessage(new MessageToWindow(newUrl: url));
        }
        while (url);
      }
    }
  });
}

/// Focuses the last created window, or if there isn't one, creates one
Future createWindow() {
  var completer = new Completer();
  var windows = app.window.getAll();
  
  if (windows.length < 1) {
    
    var windowOptions = new CreateWindowOptions(
      bounds: new ContentBounds(width: 570, height: 650)
    );
  
    app.window.create(_windowUrl, windowOptions).then((newWindow) {
      _window = newWindow;
      completer.complete(null);
    })
    .catchError((error) {
      completer.complete(null);
    });
  }
  
  else {
    windows[0].focus();
    completer.complete(null);
  }
  
  return completer.future;
}
