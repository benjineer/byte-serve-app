import 'dart:html';
import 'dart:js';
import 'package:polymer/polymer.dart';
import 'package:byte_serve_app/src/byte_serve/byte_serve.dart';
import 'package:byte_serve_app/src/downloads_view/downloads_view.dart';

DownloadsView get mainView => querySelector('#main-view') as DownloadsView;

void main() {  
  initPolymer();
  context['addNewUrl'] = addNewUrl;
  context['getRegisterFilename'] = getRegisterFilename;
  context['getRegisterBinary'] = getRegisterBinary;
}

void addNewUrl(String url) {  
  mainView.addNewDownload(url);
}

String getRegisterFilename() {
  return FileSystemRegister.filename;
}

Blob getRegisterBinary() {
  var pairs = mainView.byteServe.downloads.values;
  var blob = (mainView.byteServe.register as FileSystemRegister).getSaveBinary(pairs);
  return blob;
}
