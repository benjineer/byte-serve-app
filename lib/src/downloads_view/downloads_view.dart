import '../byte_serve/byte_serve.dart';
import 'download_control/download_control.dart';
import 'package:polymer/polymer.dart';
import 'dart:html';

@CustomTag('downloads-view')
class DownloadsView extends PolymerElement {
  
  //@observable String inputValue = 'http://localhost:7888/WebStorm-9.0.3.dmg';
  
  @observable String inputValue = 'https://rufus.akeo.ie/downloads/rufus-2.2.exe';
  
  ByteServe _byteServe;
  ByteServe get byteServe => _byteServe;
  
  ContentElement _content;
  
  DownloadsView.created() : super.created() {
    querySelector('body').classes.add('fullbleed');
  }
  
  @override
  void ready() {
    _content = shadowRoot.querySelector('content');
    _byteServe = new ByteServe();
    
    _byteServe.initialise()
    .then((_) {      
      for (var pair in _byteServe.downloads.values) {
        var state = '';
        
        if (pair.download.isCompleted) {
          state = DownloadControl.stateCompleted;
        }      
        else if (pair.download.isDownloading) {
          state = DownloadControl.stateDownloading;
        }
        else {
          state = DownloadControl.statePaused;
        }
        
        _addDownloadControl(pair.download, state);
      }
    })
    .catchError((error) {
      print(error.toString());
      
      // TODO: show ByteServe.initialise() error in view
    });
    
    _byteServe.progressEventStream.listen(update);
  }
  
  void addNewDownload(String url) {
    var download = new Download(url, _byteServe.chunkSize, connectionLimit: _byteServe.connectionLimit);
    var control = _addDownloadControl(download, DownloadControl.stateDisabled);
    
    _byteServe.addDownload(download)
    .then((download) {
      control.attributes['state'] = DownloadControl.stateDownloading;
      control.attributes['filename'] = download.filename;
    })
    .catchError((error) {
      showError(download.id, error.message);
    });
  }
  
  void handleNewUrlSubmit(Event event) {
    event.preventDefault();
    addNewDownload(inputValue);
    inputValue = '';
  }
  
  void update(ByteServeEvent event) {
    if (event.runtimeType == DownloadProgressEvent) {
      var state;
      var progressEvent = (event as DownloadProgressEvent);
      
      switch (event.name) {
        case DownloadProgressEvent.progressTypeName:
          state = DownloadControl.stateDownloading;
          break;
  
        case DownloadProgressEvent.chunkCompletedTypeName:
          state = DownloadControl.stateDownloading;
          break;
  
        case DownloadProgressEvent.downloadCompletedTypeName:
          state = DownloadControl.stateCompleted;
          break;
  
        case DownloadProgressEvent.errorTypeName:
          state = DownloadControl.stateError;
          showError(progressEvent.downloadId, progressEvent.error.message);
          break;
      }
  
      var control = shadowRoot.querySelector('#' + progressEvent.downloadId);
      DownloadControl.update(control, progressEvent.download, state);
    }
  }
  
  void showError(String downloadId, String message) {
    var control = shadowRoot.querySelector('#' + downloadId);
    control.attributes['errorMessage'] = message;
    control.attributes['state'] = DownloadControl.stateError;
  }
  
  void handleServeFile(Event event) {
    var download = _getDownloadFromEvent(event);
    _byteServe.serveFile(download);
  }
  
  void handlePause(Event event) {
    var download = _getDownloadFromEvent(event);
    _byteServe.pause(download.id);
  }
  
  void handleResume(Event event) {
    var download = _getDownloadFromEvent(event);
    _byteServe.resume(download.id);
  }
  
  void handleRetry(Event event) {
    var download = _getDownloadFromEvent(event);    
    _byteServe.retry(download.id);
  }
  
  void handleRemove(Event event) {
    var download = _getDownloadFromEvent(event);
    
    if (download != null) {
      _byteServe.remove(download.id);
    }
    (event.target as Element).remove();
  }
  
  void closeByteServe() {
    _byteServe.close();
  }
  
  Download _getDownloadFromEvent(Event event) {
    var id = (event.target as Element).id;
    return _byteServe.getDownload(id);
  }
  
  DownloadControl _addDownloadControl(Download download, String controlState) {
    var control = new DownloadControl.fromDownload(download, controlState)
      ..addEventListener('fileserve', handleServeFile)
      ..addEventListener('pause', handlePause)
      ..addEventListener('resume', handleResume)
      ..addEventListener('retry', handleRetry)
      ..addEventListener('remove', handleRemove);
    
    _content.append(control);    
    return control;
  }  
}
