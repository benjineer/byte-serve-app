import '../../byte_serve/byte_serve.dart';
import 'package:polymer/polymer.dart';
import 'dart:html';

@CustomTag('download-control')
class DownloadControl extends PolymerElement {
  
  static const String stateDisabled = 'disabled';
  static const String stateDownloading = 'downloading';
  static const String statePaused = 'paused';
  static const String stateCompleted = 'completed';
  static const String stateError = 'error';
  
  static const int buttonStatePause = 0;
  static const int buttonStateResume = 1;
  static const int buttonStateRetry = 2;
  
  @published String filename;
  @published int downloadedPercentage;
  @published String downloaded;
  @published String size;
  @published String host;
  @published String state;
  @published String errorMessage;
  
  @observable bool isInError;
  
  Element _progressBar;
  Element _fileButton;
  Element _pauseButton;
  Element _removeButton;
  
  Function _pauseButtonHandler;
  
  DownloadControl.created() : super.created();
  
  @override
  void ready() {
    _progressBar = shadowRoot.querySelector('paper-progress');
    _fileButton = shadowRoot.querySelector('#file-button');
    _pauseButton = shadowRoot.querySelector('#pause-button');
    _removeButton = shadowRoot.querySelector('#remove-button');
  }
  
  factory DownloadControl.fromDownload(Download download, String state) {    
    var control = new Element.tag('download-control')
      ..id = download.id
      ..attributes.addAll({
        'downloadedPercentage': download.getDownloadedPercentage().toString(),
        'downloaded': formatSize(download.getDownloadedSize()),
        'size': formatSize(download.size),
        'host': Uri.parse(download.url).host,
        'state': state
      });
      
    return control;
  }
  
  void stateChanged(String oldState, String newState) {    
    isInError = newState == stateError;
        
    switch (newState) {
      
      case DownloadControl.stateDisabled:
        _progressBar.attributes['disabled'] = 'disabled';
        _fileButton.attributes['disabled'] = 'disabled';
        _pauseButton.attributes['disabled'] = 'disabled';
        _removeButton.attributes['disabled'] = 'disabled';
        _updatePauseButton(buttonStatePause);
        break;
        
      case DownloadControl.stateDownloading:
        if (oldState != statePaused) {
          _enableAllButtons();
        }
        _updatePauseButton(buttonStatePause);
        break;

      case DownloadControl.statePaused:
        if (oldState != stateDownloading) {
          _enableAllButtons();
        }
        _updatePauseButton(buttonStateResume);
        break;

      case DownloadControl.stateCompleted:
        _progressBar.attributes.remove('disabled');
        _fileButton.attributes.remove('disabled');
        _pauseButton.attributes['disabled'] = 'disabled';
        _removeButton.attributes.remove('disabled');
        _updatePauseButton(buttonStatePause);
        break;

      case DownloadControl.stateError:
        _progressBar.attributes['disabled'] = 'disabled';
        _fileButton.attributes['disabled'] = 'disabled';
        _pauseButton.attributes.remove('disabled');
        _removeButton.attributes.remove('disabled');
        _updatePauseButton(buttonStateRetry);
        break;        
    }
  }
  
  void handleServeFile(Event event) {
    fire('fileserve');
  }
  
  void handlePauseButton(Event event) {
    _pauseButtonHandler(event);
  }
  
  void handlePause(Event event) {
    state = statePaused;
    fire('pause');
  }
  
  void handleResume(Event event) {
    state = stateDownloading;
    fire('resume');
  }
  
  void handleRetry(Event event) {
    state = stateDownloading;
    fire('retry');
  }
  
  void handleRemove(Event event) {
    state = stateDisabled;
    fire('remove');
  }
  
  void _enableAllButtons() {
    _progressBar.attributes.remove('disabled');
    _fileButton.attributes['disabled'] = 'disabled';
    _pauseButton.attributes.remove('disabled');
    _removeButton.attributes.remove('disabled');
  }
  
  void _updatePauseButton(int buttonState) {
    var icon;
    
    switch (buttonState) {
      case buttonStatePause:
        icon = 'av:pause';
        _pauseButtonHandler = handlePause;
        break;
        
      case buttonStateResume:
        icon = 'av:play-arrow';
        _pauseButtonHandler = handleResume;
        break;
        
      case buttonStateRetry:
        icon = 'av:replay';
        _pauseButtonHandler = handleRetry;
        break;
    }
    
    _pauseButton.attributes['icon'] = icon;
  }
  
  static String formatSize(int size, {int decimals: 1}) {
    var str = '-';
    
    if (size != null) {
      var kB = 1024;
      var MB = kB * kB;
      var GB = MB * kB;
      
      if (size < kB) {
        str = size.toString() + 'B';
      }
      else if (size < MB) {
        str = (size / kB).toStringAsFixed(decimals) + 'kB';
      }
      else if (size < GB) {
        str = (size / MB).toStringAsFixed(decimals) + 'MB';
      }
      else {
        str = (size / GB).toStringAsFixed(decimals) + 'GB';
      }
    }
    
    return str;
  }
  
  static DownloadControl update(Element downloadControl, Download download, String state) {    
    downloadControl.attributes.addAll({
      'downloadedPercentage': download.getDownloadedPercentage().toString(),
      'downloaded': formatSize(download.getDownloadedSize()),
      'size': formatSize(download.size),
      'state': state
    });
    
    return downloadControl;
  }
}