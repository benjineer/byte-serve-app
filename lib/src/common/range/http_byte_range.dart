
part of range;

class HttpByteRange extends Range {
  int start;
  int end;
  int totalSize;
  
  HttpByteRange(int start, int end, {int totalSize}) : super(start, end) {
    this.totalSize = totalSize;
  }
  
  factory HttpByteRange.fromContentRangeHeaderValue(String headerValue) {    
    if (headerValue == null) {
      return null;
    }
    
    var rex = new RegExp(r"bytes\s+(\d+)-(\d+)\s?\/\s?(\d+)");    
    var match = rex.firstMatch(headerValue);
    
    if (match != null) {
      if (match.groupCount > 2) {
        
        var startString = match.group(1);
        var endString = match.group(2);
        var totalSizeString = match.group(3);
        
        int start = int.parse(startString, onError: (_) {
          return null;
        });
        
        int end = int.parse(endString, onError: (_) {
          return null;
        });
        
        int totalSize = int.parse(totalSizeString, onError: (_) {
          return null;
        });
        
        return new HttpByteRange(start, end, totalSize: totalSize);
      }
    }
  }
  
  factory HttpByteRange.fromRangeHeaderValue(String headerValue) {
    if (headerValue == null) {
      return null;
    }
    
    var rex = new RegExp(r"bytes\s*=\s*(\d+)-(\d+)");    
    var match = rex.firstMatch(headerValue);
    
    if (match != null) {
      if (match.groupCount > 1) {
        
        var startString = match.group(1);
        var endString = match.group(2);
        
        int start = int.parse(startString, onError: (_) {
          return null;
        });
        
        int end = int.parse(endString, onError: (_) {
          return null;
        });
        
        return new HttpByteRange(start, end);
      }
    }
  }
  
  String toContentRangeHeaderValue() {
    return 'bytes ' + start.toString() + '-' + end.toString() + '/' + totalSize.toString();
  }

  
  String toRangeHeaderValue() {
    return 'bytes=' + start.toString() + '-' + end.toString();
  }
}