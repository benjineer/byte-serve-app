
library range;
part 'http_byte_range.dart';

class Range {
  int start;
  int end;
  int get length => end - start;
  
  Range(int start, int end) {
    this.start = start;
    this.end = end;
  }
  
  @override
  String toString() {
    return start.toString() + '..' + end.toString();
  }
  
  bool isWithin(Range bounds, {bool inclusive: true}) {
    if (inclusive) {
      return start >= bounds.start && end <= bounds.end;
    }
    else {
      return start > bounds.start && end < bounds.end;
    }
  }
}