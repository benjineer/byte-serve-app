part of byte_serve;

abstract class ByteServeEvent {
  String _name;
  String get name => _name;
  
  String get downloadId;
}