part of byte_serve;

class CacheWriteEvent extends ByteServeEvent {
  static const String partFileWrittenTypeName = "PART_FILE_WRITTEN";
  static const String wholeFileWrittenTypeName = "WHOLE_FILE_WRITTEN";

  String _filename;
  
  @override
  String get downloadId => _filename;
  
  DataChunk _chunk;
  DataChunk get chunk => _chunk;
  
  CacheWriteEvent.partFile(String filename, DataChunk chunk) {
    _name = partFileWrittenTypeName;
    _filename = filename;
    _chunk = chunk;
  }
  
  CacheWriteEvent.wholeFile(String filename) {
    _name = wholeFileWrittenTypeName;
    _filename = filename;
  }
}