part of byte_serve;

class DownloadProgressEvent extends ByteServeEvent {

  static const String progressTypeName = "PROGRESS";
  static const String chunkCompletedTypeName = "CHUNK_COMPLETED";
  static const String downloadCompletedTypeName = "DOWNLOAD_COMPLETED";
  static const String errorTypeName = "ERROR";

  @override
  String get downloadId => download.id;

  Download _download;
  Download get download => _download;
  
  DownloadChunk _chunk;
  DownloadChunk get chunk => _chunk;
  
  ByteServeError _error;
  ByteServeError get error => _error;
  
  ByteBuffer _data;  
  ByteBuffer get data => _data;
  
  DataChunk get dataChunk => (chunk == null || _data == null) ? null : new DataChunk(chunk.index, chunk.startPosition, _data);
  
  void set download(Download download) {
    _download = download;
  }
  
  DownloadProgressEvent(String name, {Download download, DownloadChunk chunk, ByteServeError error, ByteBuffer data}) {
    _name = name;
    _download = download;
    _chunk = chunk;
    _error = error;
    _data = data;
  }
  
  DownloadProgressEvent.progress({Download download, DownloadChunk chunk}) : this(progressTypeName, download: download, chunk: chunk);

  DownloadProgressEvent.chunkComplete({Download download, DownloadChunk chunk, ByteBuffer data}) : this(chunkCompletedTypeName, download: download, chunk: chunk, data: data);
  
  DownloadProgressEvent.downloadCompleted(Download download) : this(downloadCompletedTypeName, download: download);

  DownloadProgressEvent.errorEvent({Download download, DownloadChunk chunk, ByteServeError error}) : this(errorTypeName, download: download, chunk: chunk, error: error);
  
}