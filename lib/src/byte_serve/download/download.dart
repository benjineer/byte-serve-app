part of byte_serve;

class Download {
    
  String _url;
  String get url => _url;
  
  String _filename;
  String get filename => _filename;
  
  String _id;
  String get id => _id;
  
  int _size;
  int get size => _size;
  
  String _mime;
  String get mime => _mime;
  
  int _chunkSize;
  int get chunkSize => _chunkSize;
  
  int _connectionLimit;
  int get connectionLimit => _connectionLimit;
  
  int _completeChunkCount;
  int get completeChunkCount => _completeChunkCount;
  
  List<DownloadChunk> _chunks;
  List<DownloadChunk> get chunks => _chunks;
  
  int _nextChunkIndex;
  int get nextChunkIndex => _nextChunkIndex;

  StreamController<DownloadProgressEvent> _streamController;
  
  bool get isInitialised => size != null && mime != null;
  
  bool get isCompleted => completeChunkCount == chunks.length;
  
  bool get isDownloading {
    return chunks.where((chunk) => chunk.isDownloading).isNotEmpty;
  }
  
  Download(String url, int chunkSize, {String id, int size, String mime, List<DownloadChunk> chunks, int connectionLimit, String filename, int completeChunkCount: 0}) {
    _url = url;
    _chunkSize = chunkSize;
    _id = id == null ? generateId() : id;
    _size = size;
    _mime = mime;
    _connectionLimit = connectionLimit;
    _chunks = chunks == null ? new List<DownloadChunk>() : chunks;
    _filename = filename;
    _completeChunkCount = completeChunkCount;
    _nextChunkIndex = _chunks.length;
  }
  
  int getDownloadedSize() {
    var dlSize = 0;    
    chunks.forEach((c) => dlSize += c.bytesDownloaded);
    return dlSize;
  }
  
  int getDownloadedPercentage() {
    if (isInitialised) {
      return ((getDownloadedSize() / size) * 100).floor();
    }
    else {
      return 0;
    }
  }
  
  int getCompletedChunkSize() {
    var cSize = 0;
    chunks.where((c) => c.isCompleted).forEach((c) => cSize += c.size);
    return cSize;
  }
  
  Future initialise() {    
    var completer = new Completer();

    var request = new HttpRequest();
    request.open('GET', url);
    request.setRequestHeader('Range', new HttpByteRange(0, 1).toRangeHeaderValue());
    
    request.onLoad.listen((event) {
      var rangeError = new DownloadError('Byte serve not available', isRecoverable: false);
      var rangeString = request.getResponseHeader('Content-Range');
      var range = new HttpByteRange.fromContentRangeHeaderValue(rangeString);
      
      if (range == null) {
        completer.completeError(rangeError);
      }
      
      else {
        _size = range.totalSize;
        
        if (chunkSize > _size) {
          _chunkSize = _size;
        }
        
        _mime = request.getResponseHeader('Content-Type');
        _setFilename(request.getResponseHeader('Content-Disposition'));
        initialiseChunks();
        completer.complete(null);
      }
    });
    
    request.onError.listen((event) => completer.completeError(new DownloadError.fromHttpProgressEvent(event)));
    request.onTimeout.listen((event) => completer.completeError(new DownloadError.fromHttpProgressEvent(event)));    
    request.send();
    
    return completer.future;
  }

  Stream<DownloadProgressEvent> start() {    
    assert(mime != null && size != null && isInitialised);

    var downloadingChunks = 0;
    _streamController = new StreamController();    
    
    chunks.forEach((chunk) {
      
      if (!chunk.isCompleted) {
        _startChunk(chunk);
        ++downloadingChunks;
        ++_nextChunkIndex;
        
        if (downloadingChunks == connectionLimit) {
          return;
        }
      }
    });
    
    return _streamController.stream;
  }

  void stop() {
    
    if (_chunks != null) {
      _chunks.forEach((chunk) {
        
        if (chunk.isDownloading) {
          chunk.stop();
        }
      });
    }
    
    if (_streamController != null) {
      _streamController.close();
    }
  }
  
  void clear() {
    stop();
    initialiseChunks();
    _completeChunkCount = 0;
  }
  
  void initialiseChunks() {
    assert(size != null);
    
    _chunks = new List<DownloadChunk>();
    
    int chunkCount = (size / chunkSize).ceil();
      
    for (var i = 0; i < chunkCount; ++i) {        
      var startPos = chunkSize * i;
      var endPos = 0;
      
      if (i + 1 == chunkCount) {
        endPos = size - 1;
      }
      else {
        endPos = startPos + chunkSize - 1;
      }
          
      chunks.add(new DownloadChunk(url, i, startPos, endPos));
    }
  }
  
  void _startChunk(DownloadChunk chunk) {
    
    chunk.start().listen((event) {
      
      event.download = this;
      
      if (!_streamController.isClosed) {
        _streamController.add(event);
      }
      
      if (event.name == DownloadProgressEvent.chunkCompletedTypeName) {
        ++_completeChunkCount;
        
        if (isCompleted) {
          if (!_streamController.isClosed) {
            _streamController.add(new DownloadProgressEvent.downloadCompleted(this));
            _streamController.close();
          }
        }
        
        else {
          if (_nextChunkIndex < chunks.length) {          
            _startChunk(chunks[_nextChunkIndex]);          
            ++_nextChunkIndex;
          }
        }
      }
      
    });
  }

  void _setFilename(String contentDisposition) {
    var name = null;
   
    // Use content disposition header value as first choice
    if (contentDisposition != null) {
      var rex = new RegExp(r'filename\s*=\s*"(.+)"$');
      var match = rex.firstMatch(contentDisposition);

      if (match != null) {
        var groupCount = match.groupCount;
        
        if (groupCount > 0) {
          name = match.group(1);
        }
      }
    }
   
    // Use URL filename as second choice
    if (name == null) {
      if (url != null) {
        var uri = Uri.parse(url);
       
        if (uri != null) {
          var segments = uri.pathSegments;
         
          if (segments.length > 0) {
            name = Uri.decodeComponent(segments.last);
          }
        }
      }

      // Use fallback name as final choice
      if (name == null) {
        name = 'file';
      }
    }    
   
    _filename = name;
  }
  
  /// Returns a GUID with a 'd' prepended so it can be used as a CSS selector
  /// (CSS selectors can't start with an integer)
  static String generateId() {
    return 'd' + ((new Uuid()).v4() as String);
  }
}
