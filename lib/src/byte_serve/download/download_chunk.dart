part of byte_serve;

class DownloadChunk {

  String _url;
  int _index;
	int _startPosition;
	int _endPosition;	
	int _bytesDownloaded;
	bool _isDownloading;
	
	StreamController _streamController;
	HttpRequest _request;
	
  String get url => _url;
  int get index => _index;
  int get startPosition => _startPosition;
  int get endPosition => _endPosition;
  int get bytesDownloaded => _bytesDownloaded;
  int get size => calculateSize(startPosition, endPosition);
  bool get isDownloading => _isDownloading;
  bool get isCompleted => bytesDownloaded >= size;
  int get percentDone => ((bytesDownloaded / size) * 100).floor();
  
  DownloadChunk(String url, int index, int startPosition, int endPosition, {int bytesDownloaded: 0}) {
    _url = url;
    _index = index;
    _startPosition = startPosition;
    _endPosition = endPosition;
    _bytesDownloaded = bytesDownloaded;
    _isDownloading = false;
  }
  
  void setCompleted() {
    _bytesDownloaded = size;
  }
  
  Stream<DownloadProgressEvent> start() {
    assert(startPosition != null && endPosition != null);
    
    _bytesDownloaded = 0;
    _streamController = new StreamController<DownloadProgressEvent>();
    
    var range = new HttpByteRange(startPosition, endPosition).toRangeHeaderValue();
    
    _request = new HttpRequest()
      ..open('GET', url)
      ..responseType = 'arraybuffer'
      ..setRequestHeader('Range', range)
      
      ..onProgress.listen((ProgressEvent event) {
        if (_isDownloading) { // ignore progress when download is stopped
          _bytesDownloaded = event.loaded + 1; // event.loaded must refer to index rather than size?
          var request = event.target as HttpRequest;
          
          if (request.status > 199 && request.status < 300) {  // TODO: move HttpStatus codes
            if (!_streamController.isClosed) {
              _streamController.add(new DownloadProgressEvent.progress(chunk: this));
            }
          }
          else {
            _handleError(event);
          }
        }
      })
      
      ..onError.listen((ProgressEvent event) {
        _handleError(event);
      })
      
      ..onLoad.listen((ProgressEvent event) {      
        _bytesDownloaded = event.loaded + 1;
        _isDownloading = false;
        
        if (!_streamController.isClosed) {
          _streamController.add(new DownloadProgressEvent.chunkComplete(chunk: this, data: (event.target as HttpRequest).response));
          _streamController.close();
          _request = null;
        }
        
        if (_bytesDownloaded < size) {
          throw new DownloadError("Downloaded data is smaller than the size requested", isRecoverable: false);
          _request = null;
        }
      })
      
      ..send();
    
    _isDownloading = true;
    
    return _streamController.stream;
  }
  
  void stop() {    
    if (_isDownloading) {
      _isDownloading = false;
      
      if (_streamController != null) {
        _streamController.close();
      }
      
      if (_request != null) {
        _request.abort();
        _request = null;
      }
    }
  }
  
  void _handleError(ProgressEvent event) {
    var error = new DownloadError.fromHttpProgressEvent(event);
    if (!_streamController.isClosed) {
      _streamController.add(new DownloadProgressEvent.errorEvent(error: error, chunk: this));
    }
    stop();
  }
	
	static int calculateSize(startPosition, endPosition) {
	  return endPosition - startPosition + 1;
	}
}
