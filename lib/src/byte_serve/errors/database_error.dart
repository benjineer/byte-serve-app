part of byte_serve;

class DatabaseError extends RegisterError {
  
  DatabaseError(String message, {bool isRecoverable}) : super(message, isRecoverable: isRecoverable);
  
}