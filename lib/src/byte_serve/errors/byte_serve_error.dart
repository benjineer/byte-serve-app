part of byte_serve;

abstract class ByteServeError implements Exception {
  
  String _message;
  bool _isRecoverable;

  String get message => _message;
  bool get isRecoverable => _isRecoverable;
  
  ByteServeError(String message, {bool isRecoverable}) {
    _message = message;
    _isRecoverable = isRecoverable;
  }
  
}