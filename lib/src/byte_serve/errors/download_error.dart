part of byte_serve;

class DownloadError extends ByteServeError {
  
  DownloadError(String message, {bool isRecoverable}) : super(message, isRecoverable: isRecoverable);
  
  DownloadError.fromHttpProgressEvent(ProgressEvent event) : super(
    _messageFromEvent(event, 'Network error'),
    isRecoverable: errorIsRecoverable(event)
  );
  
  static bool errorIsRecoverable(ProgressEvent event) {
    var status = (event.target as HttpRequest).status;
    return (status > 299 && status < 400) || status > 499; // TODO: put HttpStatus codes somewhere
  }
  
  static String _messageFromEvent(ProgressEvent event, String orMessage) {
    var statusText = (event.target as HttpRequest).statusText;
    return statusText.isEmpty ? orMessage : statusText;
  }
}