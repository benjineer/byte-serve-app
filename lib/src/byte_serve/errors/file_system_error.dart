part of byte_serve;

class FileSystemError extends CacheError {
  
  //TODO - implement FileSystemError
  
  FileSystemError(String message, {bool isRecoverable}) : super(message, isRecoverable: isRecoverable);
  
  FileSystemError.recordNotFound(String name) : super.recordNotFound(name);
  
  FileSystemError.fromFileError(FileError error) : this(error.message, isRecoverable: false);
  
}