part of byte_serve;

class CacheError extends ByteServeError {
  
  CacheError(String message, {bool isRecoverable}) : super(message, isRecoverable: isRecoverable);
  
  CacheError.recordNotFound(String name)
      : super(name + ' not found in cache', isRecoverable: false);
  
}