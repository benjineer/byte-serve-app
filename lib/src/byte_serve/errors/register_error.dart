part of byte_serve;

class RegisterError extends ByteServeError {
    
  RegisterError(String message, {bool isRecoverable}) : super(message, isRecoverable: isRecoverable);
  
}