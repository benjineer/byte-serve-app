part of byte_serve;

class DownloadCachePair {
  
  Download download;
  CacheRecordBase cacheRecord;
  
  String get id => download.id;
  
  DownloadCachePair(Download download, CacheRecordBase cacheRecord) {
    this.download = download;
    this.cacheRecord = cacheRecord;
  }
  
  void stop() {
    download.stop();
  }
  
  Future clear() {
    download.clear();
    return cacheRecord.clear();
  }
  
  void destroy() {
    download.clear();
  }  
}
