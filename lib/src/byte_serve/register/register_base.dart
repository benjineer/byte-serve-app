part of byte_serve;

abstract class RegisterBase {
  
  Future initialise();
  
  Future saveDownloads(List<DownloadCachePair> pairs);
  
  Future<List<Download>> readDownloads(int newConnectionLimit);
  
  Future clear();
}