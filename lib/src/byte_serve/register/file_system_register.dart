part of byte_serve;

class FileSystemRegister extends RegisterBase {
  
  FileSystem _fileSystem;
  FileSystem get fileSystem => _fileSystem;
  
  int _size;
  
  bool _isInitialised;
  bool get isInitialised => _isInitialised;
  
  static const String filename = 'register.json';
  
  FileSystemRegister({size: 0}) {
    _size = size;
    _isInitialised = false;
  }
  
  @override
  Future clear() {
    var completer = new Completer();
    
    _fileSystem.root.getFile(filename)
    .then((file) {
      file.remove();
      completer.complete(null);
    })
    .catchError((error) {
      completer.complete(null);
    });
    
    return completer.future;
  }

  @override
  Future initialise([FileSystem fileSystem]) {
    var completer = new Completer();
        
    if (isInitialised || fileSystem != null) {
      _fileSystem = fileSystem;
      completer.complete(null);
    }
    
    else {
      window.requestFileSystem(_size, persistent: true)
      .then((fileSystem) {      
        _fileSystem = fileSystem;
        _isInitialised = true;
        completer.complete(null);
      })
      .catchError((fileError) {
        completer.completeError(new FileSystemError.fromFileError(fileError));
      });
    }
    
    return completer.future;
  }

  @override
  Future<List<Download>> readDownloads(int newConnectionLimit) {
    var completer = new Completer();
    var downloads = new List<Download>();
    
    _fileSystem.root.getFile(filename)
    .then((entry) {      
      (entry as FileEntry).file()
      
      .then((file) {
        var reader = new FileReader();
        
        reader.onLoad.first
        .then((_) {
          var codec = new Utf8Codec();
          var jsonString = codec.decode(reader.result);
            
          if (jsonString != null) {
            if (jsonString.length > 0) {
              var recordMaps = new JsonDecoder().convert(jsonString);
              
              if (recordMaps != null) {
                for (var recordMap in recordMaps) {
                  var record = new DownloadRecord();
                  record.setValuesFromMap(recordMap);
                  downloads.add(record.toDownload(newConnectionLimit));
                }
  
                completer.complete(downloads);
              }
            }
          }
          
          if (!completer.isCompleted) {
            completer.complete(downloads);
          }
        }); 
        
        reader.readAsArrayBuffer(file);
      })
      .catchError((error) {
        completer.complete(downloads);
      });
    })
    .catchError((error) {
      completer.complete(downloads);
    });
    
    return completer.future;
  }

  @override
  Future saveDownloads(List<DownloadCachePair> pairs) {
    var completer = new Completer();
    var binary = getSaveBinary(pairs);
    
    _fileSystem.root.createFile(filename, exclusive: false)
    .then((entry) {
      
      (entry as FileEntry).createWriter()
      .then((writer) {
        
        writer.write(binary);
        completer.complete(null);
      })
      .catchError((error) {
        completer.complete(null);
      });
    })
    .catchError((error) {
      completer.complete(null);
    });
    
    return completer.future;
  }
  
  Blob getSaveBinary(List<DownloadCachePair> pairs) {
    var records = new List<Map<String, dynamic>>();
        
    for (var pair in pairs) {
      records.add(new DownloadRecord.fromPair(pair).toMap());
    }
    
    var jsonString = new JsonEncoder().convert(records);    
    var codec = new Utf8Codec();
    var encoded = codec.encode(jsonString);
    
    return new Blob([encoded]);
  }
}
