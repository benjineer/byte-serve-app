part of byte_serve;

// TODO: better JSON conversion
abstract class JsonConvertable {
  
  Map toMap();
  
  void setValuesFromMap(Map map);
  
  String toJson() {
    return new JsonEncoder().convert(toMap());
  }
  
  void setValuesFromJson(String json) {
    setValuesFromMap(new JsonDecoder().convert(json));
  }
}