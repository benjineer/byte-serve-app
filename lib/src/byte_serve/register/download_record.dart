part of byte_serve;

/// Class for saving a [Download] to and restoring from disk
class DownloadRecord extends JsonConvertable {
  
  String id;
  String url;
  int chunkSize;
  int size;
  String mime;
  String filename;
  List<bool> chunkWriteState;
  
  DownloadRecord({String id, String url, int chunkSize, int size, String mime, String filename, List<bool> chunkStates}) {
    this.id = id;
    this.url = url;
    this.chunkSize = chunkSize;
    this.size = size;
    this.mime = mime;
    this.filename = filename;
    this.chunkWriteState = chunkStates == null ? new List<bool>() : chunkStates;
  }
  
  factory DownloadRecord.fromPair(DownloadCachePair pair) {
    var record = new DownloadRecord(
      id: pair.download.id,
      url: pair.download.url,
      chunkSize: pair.download.chunkSize,
      size: pair.download.size, 
      mime: pair.download.mime,
      filename: pair.download.filename,
      chunkStates: pair.cacheRecord.chunkWriteState
    );
    
    return record;
  }
  
  /// Sets [chunk.bytesDownloaded] to [chunk.size] if [chunkWriteState] is true,
  /// otherwise it sets it to 0.
  Download toDownload(int connectionLimit) {
    Download download = new Download(
      url, 
      chunkSize,
      id: id, 
      size: size, 
      mime: mime,
      connectionLimit: connectionLimit,
      filename: filename
    );
    
    download.initialiseChunks();
    
    var chunkIndex = 0;
    for (var chunk in download.chunks) {
      
      if (chunkWriteState[chunkIndex]) {
        chunk.setCompleted();
      }
           
      ++chunkIndex;
    }
    
    return download;
  }

  @override
  Map<String, dynamic> toMap() {
    var map = {
      'id': id,
      'url': url,
      'chunkSize': chunkSize,
      'size': size,
      'mime': mime,
      'filename': filename,
      'chunkStates': chunkWriteState
    };
    
    return map;
  }

  @override
  void setValuesFromMap(Map map) {
    id = map['id'];
    url = map['url'];
    chunkSize = map['chunkSize'];
    size = map['size'];
    mime = map['mime'];
    filename = map['filename'];
    chunkWriteState = map['chunkStates']; 
  }
}
