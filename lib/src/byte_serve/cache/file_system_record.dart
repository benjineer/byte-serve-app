part of byte_serve;

class FileSystemRecord extends CacheRecordBase {
  
  FileEntry _file;
  FileWriter _writer;
  int _size;
  String _mime;
  bool _isInitialised;
  List<bool> _chunkWriteState;
  int _chunkCount;

  @override
  String get mime => _mime;

  @override
  String get name => _file.name;

  @override
  int get size => _size;

  @override
  bool get isInitialised => _isInitialised;
  
  @override
  List<bool> get chunkWriteState => _chunkWriteState;
  
  @override
  void set chunkWriteState(List<bool> state) {
    _chunkWriteState = state;
    _chunkCount = state.length;
  }
  
  @override
  int get chunkCount => _chunkCount;
  
  FileEntry get file => _file;
  
  FileWriter get writer => _writer;
  
  FileSystemRecord(FileEntry file, int size, String mime, int chunkCount) {
    _file = file;
    _size = size;
    _mime = mime;
    _chunkWriteState = new List<bool>.filled(chunkCount, false);
    _isInitialised = false;
    _chunkCount = chunkCount;
  }
  
  factory FileSystemRecord.fromDownload(Download download, FileEntry file) {
    var record = new FileSystemRecord(file, download.size, download.mime, download.chunks.length);
    record.setWriteStateFromDownload(download);
    return record;
  }
  
  @override
  /// Writes 0's to file so the size is correct,
  /// and sets the write state to all false.
  Future initialise() {    
    var completer = new Completer();
    
    chunkWriteState = new List<bool>.filled(chunkCount, false);
    
    if (isInitialised) {
      completer.complete(null);
    }
    
    else {
      _file.createWriter()
      .then((writer) {
        _writer = writer;
        _writer.seek(0);
        
        _writer.onWriteEnd.first.then((event) {          
          _isInitialised = true;
          completer.complete(null);
        });
        
        _writer.onError.first.then((event) {
          completer.completeError(new FileSystemError('Couldn\'t write to file', isRecoverable: false));
        });
          
        _writer.write(new Blob([ new Uint8List(_size) ], mime));
      })
      .catchError((error) {
        completer.completeError(error);
      });
    }
    
    return completer.future;
  }

  @override
  Future<DataChunk> write(DataChunk chunk) {
    assert(_writer != null && chunk.offset != null && chunk.data != null);
    
    var completer = new Completer();
    
    _file.createWriter()
      .then((writer) {
        _writer = writer;
        _writer.seek(chunk.offset);
        
        _writer.onWriteEnd.listen((event) {
          _chunkWriteState[chunk.index] = true;
          
          if (!completer.isCompleted) {
            completer.complete(chunk);
          }
        });
        
        _writer.onError.take(1).listen((event) {
          completer.completeError(new FileSystemError('Couldn\'t write to file', isRecoverable: false));
        });
        
        _writer.write(new Blob([chunk.data], _mime));
    })
    .catchError((error) {
      completer.completeError(new FileSystemError(error));
    });
    
    return completer.future;
  }

  @override
  String getDataUrl() {
    assert(_file != null);
    return _file.toUrl();   
  }
  
  @override
  Future<List<int>> getFileData() {
    assert(_file != null);    
    var completer = new Completer();
       
    _file.file()
    .then((file) { 
      var reader = new FileReader();
      
      reader.onLoadEnd.first
      .then((ProgressEvent event) {
        var data = reader.result;
        completer.complete(data);
      }); 
      
      reader.readAsArrayBuffer(file);
    });
    
    return completer.future;
  }

  @override
  Future destroy() {
    var completer = new Completer();
    
    _file.remove().then((_) {
      _file = null;
      completer.complete(null);
    })
    .catchError((error) {
      completer.completeError(new FileSystemError(error));
    });
    
    return completer.future;
  }

  @override
  Future clear() {
    var completer = new Completer();
    
    _isInitialised = false;
    initialise()
    .then((_) {
      completer.complete(null);
    })
    .catchError((_) {
      completer.complete(null);
    });
    
    return completer.future;
  }
}
