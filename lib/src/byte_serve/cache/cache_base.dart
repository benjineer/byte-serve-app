part of byte_serve;

abstract class CacheBase {
  
  /// Map<downloadId, record>
  Map<String, CacheRecordBase> _records;
  
  CacheBase() {
    _records = new Map<String, CacheRecordBase>();
  }
  
  void removeAll(List<String> names) {
    names.forEach((name) {
      removeRecord(name);
    });
  }
  
  void removeAllExcept(List<String> names) {
    names.forEach((name) {      
      if (!_records.containsKey(name)) {
        removeRecord(name);
      }
    });
  }
  
  bool get isInitialised;
  
  Future initialise();
  
  Future<CacheRecordBase> addRecord(String name, int size, String mime, int chunkCount);

  Future removeRecord(String name);

  Future<CacheRecordBase> findRecord(String name);
  
  Future<DataChunk> writeToRecord(String recordName, DataChunk chunk);
}