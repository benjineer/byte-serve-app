part of byte_serve;

abstract class CacheRecordBase {
  
  String get name;
  int get size;
  String get mime;
  bool get isInitialised;
  List<bool> get chunkWriteState;
  int get chunkCount;
  
  void set chunkWriteState(List<bool> state);
  
  bool get writingIsCompleted => !chunkWriteState.any((state) => state == false);
  
  Future initialise();

  Future<DataChunk> write(DataChunk chunk);

	String getDataUrl();
	
	Future<List<int>> getFileData();

	Future destroy();
  
	Future clear();
	
	void setWriteStateFromDownload(Download download) {
	  chunkWriteState = new List<bool>.filled(download.chunks.length, false);
	  
	  for (var i = 0; i < download.chunks.length; ++i) {
	    chunkWriteState[i] = download.chunks[i].isCompleted;
	  }
	}
}