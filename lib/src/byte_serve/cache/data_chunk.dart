part of byte_serve;

class DataChunk {
  
  int index;
  int offset;
  ByteBuffer data;
  
  DataChunk(int index, int offset, ByteBuffer data) {
    this.index = index;
    this.offset = offset;
    this.data = data;
  }
}