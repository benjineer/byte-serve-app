part of byte_serve;

class FileSystemCache extends CacheBase {

	FileSystem _fileSystem;
	int _size;
	bool _isInitialised;

  @override
  bool get isInitialised => _isInitialised;
  
  FileSystem get fileSystem =>_fileSystem;
  
  int get size => _size;
	
	FileSystemCache({int size: 0}) {	  
	  _size = size;
	  _isInitialised = false;
	}
	
	@override
	Future initialise([FileSystem fileSystem]) {	  
	  var completer = new Completer();
	  
	  if (isInitialised || fileSystem != null) {
	    _fileSystem = fileSystem;
	    completer.complete(null);
	  }
	  
	  else {
      window.requestFileSystem(_size, persistent: true)
      .then((fileSystem) {      
        _fileSystem = fileSystem;
        _isInitialised = true;
        completer.complete(null);
      })
      .catchError((fileError) {
        completer.completeError(new FileSystemError.fromFileError(fileError));
      });
	  }
    
    return completer.future;
	}

	/// Adds and initialises a record
  @override
  Future<CacheRecordBase> addRecord(String name, int size, String mime, int chunkCount) {    
    assert(_fileSystem != null && name != null && size != null && mime != null);
    
    var completer = new Completer<CacheRecordBase>();
    
    _fileSystem.root.createFile(name, exclusive: false)
    .then((file) {
      var record = new FileSystemRecord(file, size, mime, chunkCount);      
      _records[name] = record;
      
      record.initialise()
      .then((_) {
        completer.complete(record);          
      })
      .catchError((error) {
        completer.completeError(error);
      });
      
    })
    .catchError((fileError) {
      completer.completeError(new FileSystemError.fromFileError(fileError));
    });
    
    return completer.future;
  }

  @override
  Future<CacheRecordBase> findRecord(String name) {    
    assert(name != null);
    
    var completer = new Completer<CacheRecordBase>();
    
    if (_records.containsKey(name)) {
      completer.complete(_records[name]);
    }
    else {
      completer.complete(null);
    }
    
    return completer.future;
  }

  @override
  Future removeRecord(String name) {
    assert(name != null);
    
    var completer = new Completer();
    
    findRecord(name)
    .then((record) {
      
      record.destroy()
      .then((_) {
        _records.remove(name);
        completer.complete(null);
      })
      .catchError((fileSystemError) {
        completer.completeError(fileSystemError);
      });      
    })
    .catchError((fileSystemError) {
      completer.completeError(new FileSystemError.recordNotFound(name));
    });
    
    return completer.future;
  }

  @override
  Future<DataChunk> writeToRecord(String recordName, DataChunk chunk) {
    assert(_fileSystem != null && recordName != null && chunk != null);
    
    var completer = new Completer();
    
    findRecord(recordName)
    .then((record) {      
      
      if (record != null) {
        record.write(chunk)
        .then((_) {
          completer.complete(chunk);
        })
        .catchError((fileError) {
          completer.completeError(new FileSystemError.fromFileError(fileError)); // write error
        });
      }
      
      else {
        completer.complete(null);
      }      
    })
    .catchError((fileSystemError) {
      completer.completeError(fileSystemError); // find error
    });
    
    return completer.future;
  }
}
