library byte_serve;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'dart:typed_data';
import 'package:uuid/uuid.dart';
import '../common/range/range.dart';

part 'download_cache_pair.dart';

part 'download/download.dart';
part 'download/download_chunk.dart';

part 'cache/cache_base.dart';
part 'cache/cache_record_base.dart';
part 'cache/file_system_cache.dart';
part 'cache/file_system_record.dart';
part 'cache/data_chunk.dart';

part 'register/download_record.dart';
part 'register/file_system_register.dart';
part 'register/json_convertable.dart';
part 'register/register_base.dart';

part 'events/byte_serve_event.dart';
part 'events/cache_write_event.dart';
part 'events/download_progress_event.dart';

part 'errors/byte_serve_error.dart';
part 'errors/download_error.dart';
part 'errors/cache_error.dart';
part 'errors/file_system_error.dart';
part 'errors/register_error.dart';
part 'errors/database_error.dart';

class ByteServe {
  
  static const int mb = 1024 * 1024;
  static const int defaultChunkSize = 1 * mb;
  static const int defaultConnectionLimit = 2;

  int _chunkSize;
  int _connectionLimit;
	CacheBase _cache;
	RegisterBase _register;
	
	bool serveOnCompletion;
	
	Map<String, DownloadCachePair> _downloads;
	StreamController<ByteServeEvent> _streamController;
	
	int get chunkSize => _chunkSize;
	int get connectionLimit => _connectionLimit;
	CacheBase get cache => _cache;
	RegisterBase get register => _register;
	
	Map<String, DownloadCachePair> get downloads => _downloads;
  Stream<DownloadProgressEvent> get progressEventStream => _streamController.stream.asBroadcastStream();
  
	ByteServe({int chunkSize: defaultChunkSize, int connectionLimit: defaultConnectionLimit, bool serveOnCompletion: true}) {	  
	  _chunkSize = chunkSize;
	  _connectionLimit = connectionLimit;
    _cache = new FileSystemCache();
    _register = register == null ? new FileSystemRegister() : register;
    _streamController = new StreamController();
    _downloads = new Map<String, DownloadCachePair>();
    this.serveOnCompletion = serveOnCompletion;
	}
	
	Future initialise() {
	  var completer = new Completer();
	  
	  _register.initialise()
    .then((_) {
      
      _cache.initialise()
      .then((_) {
        
        _register.readDownloads(connectionLimit)
        .then((registerDownloads) {
          
          if (registerDownloads.length == 0) {
            completer.complete(null);
          }
          
          else {
            for (var i = 0; i < registerDownloads.length; ++i) {
              var download = registerDownloads[i];
              
              _cache.findRecord(download.id)
              .then((cacheRecord) {
                
                if (cacheRecord == null) {                    
                  _addInitialisedDownload(download)
                  .then((pair) {
                    _downloads[download.id] = pair;
                    
                    if (i + 1 == registerDownloads.length) {
                      _cache.removeAllExcept(_downloads.keys.toList(growable: false));            
                      completer.complete(null);
                    }
                  });
                }
                
                else {
                  _downloads[download.id] = new DownloadCachePair(download, cacheRecord);
                  
                  if (i + 1 == registerDownloads.length) {
                    _cache.removeAllExcept(_downloads.keys.toList(growable: false));            
                    completer.complete(null);
                  }
                }
              })
              .catchError((cacheError) {
                completer.completeError(cacheError);
              });
            }
          }
        });
      })
      .catchError((cacheError) {
        completer.completeError(cacheError);
      });
      
    })
    .catchError((registerError) {
      completer.completeError(registerError);
    });
	  
	  return completer.future;
	}
	
	Future<Download> addDownload(Download download) {
	  var completer = new Completer();	  
	  
	  download.initialise()
	  .then((_) {
	    
	    _addInitialisedDownload(download)
	    .then((downloadRecord) {	
	      
  	    download.start()
  	    .listen(_handleDownloadEvents);
  	    
  	    _downloads[download.id] = downloadRecord;
  	    completer.complete(download);
	    })
	    .catchError((cacheError) {
	      completer.completeError(cacheError);
	    });
	    
	  })
	  .catchError((downloadError) {
	    completer.completeError(downloadError);
	  });
	  
	  return completer.future;
	}
	
	Download getDownload(String id) {
	  var pair = _downloads[id];
	  return pair == null ? null : pair.download;
	}
	
	void pause(String id) {
	  _downloads[id].download.stop();
	}
	
	void resume(String id) {
	  _downloads[id].download.start()
	  .listen(_handleDownloadEvents);
  }
  
	void retry(String id) {
	  _downloads[id].clear();
	  resume(id);
  }
  
  Future remove(String id) {
    var completer = new Completer();
    _downloads[id].destroy();
    _downloads.remove(id);
    
    _cache.removeRecord(id)
    .then((_) {
      completer.complete(null);
    })
    .catchError((cacheError) {
      completer.completeError(cacheError);
    });
    
    return completer.future;
  }
  
  Future serveFile(Download download) {
	  var completer = new Completer();
	  	  
	  _cache.findRecord(download.id)
	  .then((record) {
      
      new HttpRequest()
        ..open('GET', record.getDataUrl())
        ..responseType = 'arraybuffer'
        
        ..onLoad.listen((event) {
          var dataUrl = Url.createObjectUrl(new Blob([ event.currentTarget.response ], download.mime));
          
          new AnchorElement()
            ..style.display = 'none'
            ..download = download.filename
            ..href = dataUrl
            ..click();
          
          Url.revokeObjectUrl(dataUrl);
          completer.complete(null);
        })
        
        ..send();
	  })
	  .catchError((cacheError) {
	    completer.completeError(cacheError);
	  });
	  
	  return completer.future;
	}
  
  Future close() {
    for (var pair in _downloads.values) {
      pair.download.stop();
    }
    return _register.saveDownloads(_downloads.values.toList(growable: false));
  }
  
  /// Add download to cache and register and return a record
  Future<DownloadCachePair> _addInitialisedDownload(Download download) {
    var completer = new Completer();
    
    _cache.addRecord(download.id, download.size, download.mime, download.chunks.length)
    .then((cacheRecord) {
      cacheRecord.setWriteStateFromDownload(download);
      completer.complete(new DownloadCachePair(download, cacheRecord));
    })
    .catchError((cacheError) {
      completer.completeError(cacheError);
    });

    return completer.future;
  }
	
  /// Fires all download events to _streamController and writes to cache and register
  /// Serves file when done
	void _handleDownloadEvents(ByteServeEvent event) {
    _streamController.add(event);    
    var downloadEvent = (event as DownloadProgressEvent);
        
    switch (event.name) {
      case DownloadProgressEvent.chunkCompletedTypeName:
        
        _cache.writeToRecord(downloadEvent.downloadId, downloadEvent.dataChunk)
        .then((chunk) {
          _streamController.add(new CacheWriteEvent.partFile(downloadEvent.downloadId, chunk));
          
          var record = downloads[downloadEvent.downloadId].cacheRecord;
          
          if (record.writingIsCompleted) {
            _streamController.add(new CacheWriteEvent.wholeFile(downloadEvent.downloadId));
            
            if (serveOnCompletion) {
              serveFile(downloadEvent.download);
            }
          }
        })
        .catchError((cacheError) {        
          var errorEvent = new DownloadProgressEvent.errorEvent(download: downloadEvent.download, chunk: downloadEvent.chunk, error: cacheError);
          _streamController.add(errorEvent);
        });
        
        break;
        
      case DownloadProgressEvent.errorTypeName:        
        if (downloadEvent.error.isRecoverable) {
          //TODO: handle recoverable errors (i.e. chunk / download retry)
          
          downloadEvent.download.stop();
        }
        else {
          downloadEvent.download.stop();
        }
        break;
    }
	}
}