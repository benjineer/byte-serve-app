# TODO #
0. Background page owns ByteServe instance, window and background scripts trade message to handle events
0. Fix re-open -> add download -> set filename
0. Fix re-open -> restart -> corrupt
0. Rewrite in pure JS
   0. Node?
0. Show non-download-specific error message
0. Handle download errors
    a. Tests for DownloadError class 
    a. Error handling in other classes
        i. Chunk & download retry 
        i. HTTP redirect codes
0. Implement logging
0. Add clear all button
0. Run tests as Chrome app
0. Create settings page
    a. Chunk size
    a. Limit number of concurrent downloads
    a. Connection timeout
    a. Limit chunk error retry count
    a. Remember window position and size
0. Save data on progress - requires Dart issue Issue 21126 to be resolved, or use JsObject XHR
0. Use Range class everywhere
0. Implement Common/HttpHeaders and use everywhere
0. Group tests by object state (i.e. Download initialised, DownloadChunk paused etc.) and write setup and teardown functions
0. Implement remaining tests
