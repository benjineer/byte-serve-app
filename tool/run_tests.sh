#! /bin/bash

SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
APP_ROOT="$SCRIPT_DIR/.."
TESTS_FILE="$APP_ROOT/test/all_test.dart"

# launch test server and get it's PID
eval "(dart tool/test_server.dart) &"
SERVER_PID=$!

# run tests and close server
pub run test $TESTS_FILE -p dartium
kill $SERVER_PID

