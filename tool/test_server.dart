import 'dart:io';
import '../test/test_server_settings.dart';
import '../lib/src/common/range/range.dart';

const String contentDispositionKey = 'Content-Disposition';
const String allowOriginKey = 'Access-Control-Allow-Origin';
const String allowHeadersKey = 'Access-Control-Allow-Headers';
const String exposeHeadersKey = 'Access-Control-Expose-Headers';
const String allowCredentialsKey = 'Access-Control-Allow-Credentials';

const String acceptedRanges = 'bytes';
const String allowedHeaders = 'Origin, X-Requested-With, Content-Type, Accept, Range';

const int badRequestStatus = 400;

final Range validRange = new Range(0, TestServerSettings.downloadFileSize - 1);

const String fileDir = 'bin/';
final String filePath = fileDir + TestServerSettings.downloadFileName;

HttpRequest _request;
Map<String, String> _defaultHeaders;

main() {
  run();
}

void run() {
  _defaultHeaders = {
    allowHeadersKey: allowedHeaders,
    HttpHeaders.CONTENT_TYPE: TestServerSettings.downloadFileMime,
    contentDispositionKey: fileDisposition(TestServerSettings.downloadFileName),
    allowOriginKey: '*',
    HttpHeaders.ACCEPT_RANGES: acceptedRanges,
    HttpHeaders.CONTENT_RANGE: new HttpByteRange(0, TestServerSettings.downloadFileSize - 1, totalSize: TestServerSettings.downloadFileSize).toContentRangeHeaderValue(),
    exposeHeadersKey: HttpHeaders.CONTENT_RANGE
  };
  
  HttpServer.bind(new InternetAddress(TestServerSettings.ipAddress), TestServerSettings.port)
  .then((HttpServer server) {
    
    print('Server running at ' + server.address.host + ':' + server.port.toString());
    print('ctrl+c to stop');
    
    server.listen((HttpRequest request) {
      print(request.uri.toString());
      
      var testName = request.uri.queryParameters[TestServerSettings.testQueryName];
      _request = request;
      
      if (testName != null) {
        switch (testName.toLowerCase()) {
          
          case TestServerSettings.testOkValue:
            _testOk();
            break;            
        }
      }
      
      
    });
  })
  .catchError((e) {
    print(e.toString());
  });  
}

void _serveFile({int status, Map<String, String> headers, Range range}) {
  
  if (status != null) {
    _request.response.statusCode = status;
  }
  
  if (headers != null) {
    headers.forEach((name, value) {
      _request.response.headers.add(name, value);
    });
  }
  
  var startPos = 0;
  var endPos = TestServerSettings.downloadFileData.length;
  
  if (range != null) {
    
    if (!range.isWithin(validRange)) {
      print('invalid range: ${range.toString()}');
      _request.response.statusCode = badRequestStatus;
      _request.response.close();
      return;
    }
    
    startPos = range.start;
    endPos = range.end;
    
    print(range.toString());
  }

   var data = TestServerSettings.downloadFileData.toList().getRange(startPos, endPos);
  _request.response.add(data);
  _request.response.close();
}

void _testOk() {
  var responseHeaders = _defaultHeaders;
  var requestedRange = new HttpByteRange.fromRangeHeaderValue(_request.headers.value('Range'));
  
  if (requestedRange != null) {
    requestedRange.totalSize = TestServerSettings.downloadFileSize;  
    responseHeaders[HttpHeaders.CONTENT_RANGE] = requestedRange.toContentRangeHeaderValue();
  }
  
  _serveFile(status: HttpStatus.PARTIAL_CONTENT, headers: responseHeaders, range: requestedRange);
}

String fileDisposition(String filename) {
  return 'attachment; filename="' + filename + '"';
}





