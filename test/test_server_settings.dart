
import 'dart:typed_data';
import 'package:crypto/crypto.dart';

class TestServerSettings {
  static const String ipAddress  = '127.0.0.1';
  static const int port = 1982;
  
  static const String testQueryName = 'test';
  
  static const String testOkValue = 'ok';
  
  // Length in bytes needs to be at least 100 for % done test
  static Int8List get downloadFileData {
    return new Int8List.fromList([
      1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
      3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
      4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
      5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
      6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
      7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
      8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ]);
  }
  
  static final int contiguousChunkSize = (downloadFileSize ~/ 10).toInt();

  static int get downloadFileSize => downloadFileData.lengthInBytes;
  
  static const String downloadFileMime = 'application/octet-stream';
  
  static const String downloadFileName = 'test.exe';
  
  static String get downloadFileMd5 {
    var md5 = new MD5();
    md5.add(downloadFileData);
    
    var hashBytes = md5.close();
    return CryptoUtils.bytesToHex(hashBytes);
  }
  
  static Uri testAddress(String name) {
    return new Uri.http(ipAddress + ':' + port.toString(), '/', { testQueryName: name });
  }
}
