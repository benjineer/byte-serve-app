
part of byte_serve.tests;

httpByteRangeTests() {
  test('toRangeHeaderValue returns a correctly formatted string', () {      
    var actual = new HttpByteRange(1, 1023).toRangeHeaderValue();
    var expected = 'bytes=1-1023';    
    expect(actual, equals(expected));
  });
  
  test('toContentRangeHeaderValue returns a correctly formatted string', () {      
    var actual = new HttpByteRange(1, 1023, totalSize: 2000).toContentRangeHeaderValue();
    var expected = 'bytes 1-1023/2000';    
    expect(actual, equals(expected));
  });
  
  test('fromRangeHeaderValue returns a correctly populated range', () {
    var range = new HttpByteRange.fromRangeHeaderValue('bytes=1-1023');    
    expect(range.start, equals(1));
    expect(range.end, equals(1023));
    expect(range.totalSize, isNull);
  });
  
  test('fromContentRangeHeaderValue returns a correctly formatted string', () {      
    var range = new HttpByteRange.fromContentRangeHeaderValue('bytes 1-1023/2000');    
    expect(range.start, equals(1));
    expect(range.end, equals(1023));
    expect(range.totalSize, equals(2000));
  });
}