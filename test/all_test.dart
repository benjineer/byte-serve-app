library byte_serve.tests;

@TestOn("dartium")
@Timeout(const Duration(seconds: 2))

import 'package:byte_serve_app/src/byte_serve/byte_serve.dart';
import 'package:byte_serve_app/src/common/range/range.dart';
import 'test_server_settings.dart';
import 'package:test/test.dart';
import 'package:crypto/crypto.dart';
import 'dart:async';

part 'byte_serve/byte_serve_test.dart';

part 'byte_serve/cache/file_system_cache_test.dart';
part 'byte_serve/cache/file_system_record_test.dart';

part 'byte_serve/download/download_chunk_test.dart';
part 'byte_serve/download/download_test.dart';

part 'byte_serve/register/download_record_tests.dart';

part 'common/http_byte_range_test.dart';

part 'fixtures/download_chunk_fixture.dart';
part 'fixtures/download_fixture.dart';

String okUrl = TestServerSettings.testAddress(TestServerSettings.testOkValue).toString();
  

main() {
  
  group('Common', () {
    group('HttpByteRange', httpByteRangeTests);
  });
  
  group('ByteServe', () {
    byteServeTests();
    
    group('Download', () {
      group('DownloadChunk', downloadChunkTests);
      group('Download', downloadTests);      
    });
    
    group('Cache', () {
      group('FileSystemCache', fileSystemCacheTests);
      group('FileSystemRecord', fileSystemRecordTests);
    });
    
    group('register', () {
      downloadRecordTests();
    });
    
    group('events', () {
      
    });
    
    group('errors', () {
      
    });
  });
  
  group('downloads_view', () {
    
    group('downloads_view', () {
      
    });
    
    group('downloads_control', () {
        
    });
  });
}