part of byte_serve.tests;

fileSystemCacheTests() {
  group('constructor', () {
    test('size is set', () {
      
    });
    
    test('isInitialised is set', () {
      
    });
    
    test('records is set', () {
      
    });
  });
  
  group('initialise()', () {
    test('creates a file system', () {
      
    });
  });
  
  group('add record', () {
    test('adds the record to \'records\'', () {
      
    });
    
    test('initialises the record', () {
      
    });
  });
  
  group('find record', () {
    test('returns the record if found', () {
      
    });
    
    test('returns null if not found', () {
      
    });
  });
  
  group('remove record', () {
    test('destroys the record', () {
      
    });
    
    test('removes the record from \'records\'', () {
      
    });
  });
  
  group('writeToRecord()', () {
    test('writes to correct record', () {
      
    });
    
    test('returned future returns a data chunk if the file was found', () {
      
    });
    
    test('returned future returns a null if the file wasn\'t found', () {
      
    });
  });
}