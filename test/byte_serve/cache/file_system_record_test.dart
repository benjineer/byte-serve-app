part of byte_serve.tests;

fileSystemRecordTests() {
  group('constructor', () {
    test('file is set', () {
      
    });
    
    test('size is set', () {
      
    });
    
    test('mime is set', () {
      
    });
    
    test('isInitialised is set', () {
      
    });
  });
  
  group('initialise()', () {
    test('creates a file writer', () {
      
    });
    
    test('writes a file of the correct size', () {
      
    });
  });
  
  group('write()', () {
    test('writes chunk data to the correct section of the file', () {
      
    });
  });
  
  group('getDataUrl()', () {
    test('returns a data URL', () {
      
    });
    
    test('returned URL can be accessed', () {
      
    });
  });
  
  group('getFile()', () {
    test('returns a file object', () {
      
    });
    
    test('returned file has the correct name', () {
      
    });
    
    test('returned file has the correct mime type', () {
      
    });
    
    test('returned file has the correct size', () {
      
    });
  });
  
  group('destroy', () {
    test('removes file', () {
      
    });
  });
  
  group('clear', () {
    test('clears data from file', () {
      
    });
  });
}