part of byte_serve.tests;

downloadRecordTests() {
  /*
  var chunks = new List<DownloadChunk>();
  chunks.add(new DownloadChunk(okUrl, 0, 0, 1023, bytesDownloaded: 1024));
  chunks.add(new DownloadChunk(okUrl, 1, 1024, 2000, bytesDownloaded: 0));
  
  var originalDownload = new Download(
    okUrl, 
    1024, 
    size: 2048, 
    chunks: chunks, 
    completeChunkCount: 1, 
    connectionLimit: 4, 
    id: Download.generateId(), 
    mime: 'p00t'
  );
  
  DownloadRecord downloadRecord = new DownloadRecord.fromDownload(originalDownload);  
  Download convertedDownload = downloadRecord.toDownload(originalDownload.connectionLimit);
  
  test('fromDownload() converts all fields', () {
    expect(downloadRecord.id, equals(originalDownload.id), reason: 'id');
    expect(downloadRecord.url, equals(originalDownload.url), reason: 'url');
    expect(downloadRecord.chunkSize, equals(originalDownload.chunkSize), reason: 'chunkSize');
    expect(downloadRecord.size, equals(originalDownload.size), reason: 'size');
    expect(downloadRecord.mime, equals(originalDownload.mime), reason: 'size');
    expect(downloadRecord.chunkStates.length, equals(originalDownload.chunks.length), reason: 'chunks.length');
    
    for (var i = 0; i < downloadRecord.chunkStates.length; ++i) {
      var recordChunkState = downloadRecord.chunkStates[i];
      var expectedState = originalDownload.chunks[i].isCompleted;
      
      expect(recordChunkState, equals(expectedState), reason: 'chunk' + i.toString() + ' state');
    }
  });

  test('toDownload() converts all fields', () {
    expect(convertedDownload.chunkSize, equals(downloadRecord.chunkSize), reason: 'chunkSize');
    expect(convertedDownload.id, equals(downloadRecord.id), reason: 'id');
    expect(convertedDownload.size, equals(downloadRecord.size), reason: 'size');
    expect(convertedDownload.mime, equals(downloadRecord.mime), reason: 'mime');
    expect(convertedDownload.chunks.length, equals(downloadRecord.chunkStates.length), reason: 'chunks.length');
    
    for (var i = 0; i < convertedDownload.chunks.length; ++i) {
      var downloadChunk = convertedDownload.chunks[i];
      var recordChunk = downloadRecord.chunkStates[i];
      var bytesDownloaded = downloadChunk.isCompleted ? downloadChunk.size : 0;
      
      expect(downloadChunk.index, equals(downloadChunk.index), reason: 'index');
      expect(downloadChunk.startPosition, equals(downloadChunk.startPosition), reason: 'startPosition');
      expect(downloadChunk.endPosition, equals(downloadChunk.endPosition), reason: 'endPosition');
      expect(downloadChunk.bytesDownloaded, equals(bytesDownloaded), reason: 'bytesDownloaded');
      expect(downloadChunk.isDownloading, isFalse, reason: 'isDownloading');
      expect(downloadChunk.isCompleted, equals(downloadChunk.isCompleted), reason: 'isCompleted');
    }
  });
  */
}
