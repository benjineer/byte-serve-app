part of byte_serve.tests;

downloadTests() {  

  group('constructor', () {
    test('not started state is ok', () {
      var chunkSize = 100;
      var download = new Download(okUrl, chunkSize);
      
      expect(download.url, equals(okUrl), reason: 'url');
      expect(download.chunkSize, equals(chunkSize));
      expect(download.id, startsWith('d'), reason: 'id');
      expect(download.chunks.length, isZero, reason: 'chunks length');
      expect(download.completeChunkCount, isZero, reason: 'completeChunkCount');
      expect(download.isInitialised, isFalse, reason: 'isInitialised');
      expect(download.nextChunkIndex, isZero, reason: 'nextChunkIndex');
    });
    
    test('completed state is ok', () {
      var chunks = completedChunks();
      var downloadId = Download.generateId();
      var download = completedDownload(downloadId);
      
      expect(download.id, equals(downloadId), reason: 'id');
      expect(download.url, equals(okUrl), reason: 'url');
      expect(download.size, equals(TestServerSettings.downloadFileSize), reason: 'size');
      expect(download.mime, equals(TestServerSettings.downloadFileMime), reason: 'mime');
      expect(download.chunkSize, equals(completedDownloadChunkSize), reason: 'chunkSize');
      expect(download.connectionLimit, equals(downloadConnectionLimit), reason: 'connectionLimit');
      expect(download.chunks.length, equals(chunks.length), reason: 'chunks length');
      expect(download.completeChunkCount, equals(chunks.length), reason: 'completeChunkCount');
      expect(download.isInitialised, isTrue, reason: 'isInitialised');
      expect(download.nextChunkIndex, equals(chunks.length), reason: 'nextChunkIndex');
    });
  });
  
  group('getter methods', () {
    test('getDownloadedSize() calculates correctly', () {
      
    });
      
    test('getDownloadedPercentage() calculates correctly', () {
      
    });    

    test('getCompletedChunkSize() calculates correctly', () {
      
    });
    
    test('isCompleted is true if all chunks are completed', () {
      
    });
    
    test('isDownloading returns true if any chunks are downloading', () {
      
    });
    

    test('filename returns the correct value', () {
      // uses Content-Disposition, url and "file" in that order
    });
  });
  
  group('initialise()', () {
    group('success', () {
      var download = unInitialisedDownload();
      
      test('sets isInitialised', () {        
        return download.initialise().then((_) {
          expect(download.isInitialised, isTrue);
        });
      });
      
      test('sets size', () {
        expect(download.size, equals(TestServerSettings.downloadFileSize));
      });
      
      test('limits chunk size to the size of the download', () {
        
      });
      
      test('sets mime', () {
        expect(download.mime, equals(TestServerSettings.downloadFileMime));
      });
      
      test('sets filename', () {
        
      });
    });
    
    group('failure', () {
      
    });
  });
  
  group('start()', () {
    group('success', () {
      var download = initialisedDownload();
      var progressEvents = new List<DownloadProgressEvent>();
      
      test('throws no errors', () {
        var completer = new Completer();
        
        download.start()
        .listen(
          (DownloadProgressEvent event) => progressEvents.add(event),
          onDone: () => completer.complete(null),
          
          onError: (error) {
            fail(error.message);
            completer.complete(null);
          }
        );
        
        return completer.future;
      });
      
      test('returns at least 2 progress events for each chunk', () {
        var progEvents = progressEvents.where((event) {
          return event.name == DownloadProgressEvent.progressTypeName;
        });
        
        var minCount = download.chunks.length;
        
        expect(progEvents.length, greaterThanOrEqualTo(minCount));
      });
      
      test('returns no error events', () {
        var errorEvents = progressEvents.where((event) {
          return event.name == DownloadProgressEvent.errorTypeName;
        });
        
        expect(errorEvents.length, isZero);
      });
      
      test('returns a chunk completion event for each chunk', () {
        var chunkCompleteEvents = progressEvents.where((event) {
          return event.name == DownloadProgressEvent.chunkCompletedTypeName;
        });
        
        expect(chunkCompleteEvents.length, equals(download.chunks.length));
      });
      
      test('returns a single download completion event', () {
        var completionEvents = progressEvents.where((event) {
          return event.name == DownloadProgressEvent.downloadCompletedTypeName;
        });
        
        expect(completionEvents.length, equals(1));
      });
      
      test('honours connection limit', () {
        
      });
      
      test('next chunk index equals connection limit immediately after start is called', () {
              
      });
      
      test('serves file on completion', () {
        
      });
    });
    
    group('failure', () {
      
    });
  });
  
  group('re-start()', () {
      group('success', () {
        
      });
      
      group('failure', () {
        
      });
  });
  
  group('stop()', () {
    group('is running', () {
      
    });
    
    group('is already stopped', () {
      
    });
    
    group('never started', () {
      
    });
  });
  
  group('static methods', () {
    test('generateId() returns a string stating with d', () {
      var id = Download.generateId();      
      expect(id, startsWith('d'));
    });
    
    test('generateId() returns a unique string each time out of 1000 calls', () {
      var n = 1000;
      var ids = new Set<String>();
      
      for (var i = 0; i < n; ++i) {
        ids.add(Download.generateId());
      }
      
      expect(ids.length, equals(n));
    });
    
    test('sizeFromContentRangeValue() parses contant range values correctly', () {
      
    });
  });
}