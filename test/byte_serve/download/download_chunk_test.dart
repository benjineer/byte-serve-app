part of byte_serve.tests;

downloadChunkTests() {

  group('properties', () {
    var chunkSize = TestServerSettings.downloadFileSize;
    var chunk = new DownloadChunk(okUrl, 0, 0, chunkSize - 1, bytesDownloaded: chunkSize - 1);
    
    test('size calculates correctly', () {
      expect(chunk.size, equals(chunkSize));
    });
    
    test('percentDone rounds down to the nearest whole', () {
      expect(chunk.percentDone, equals(99));
    });
  });
  
  group('constructor', () {
    test('not started state is ok', () {
      var chunkSize = TestServerSettings.contiguousChunkSize;
      var chunk = new DownloadChunk(okUrl, 0, 0, chunkSize - 1);
      
      expect(chunk.url, equals(okUrl), reason: 'url');
      expect(chunk.index, isZero, reason: 'index');
      expect(chunk.startPosition, isZero, reason: 'startPosition');
      expect(chunk.endPosition, equals(chunkSize - 1), reason: 'endPosition');
      expect(chunk.bytesDownloaded, isZero, reason: 'bytesDownloaded');
      expect(chunk.isDownloading, isFalse, reason: 'isDownloading');
      expect(chunk.isCompleted, isFalse, reason: 'isCompleted');
    });
    
    test('completed state is ok', () {
      var chunkSize = TestServerSettings.contiguousChunkSize;
      var chunk = new DownloadChunk(okUrl, 0, 0, chunkSize - 1, bytesDownloaded: chunkSize);
      
      expect(chunk.url, equals(okUrl), reason: 'url');
      expect(chunk.index, isZero, reason: 'index');
      expect(chunk.startPosition, isZero, reason: 'startPosition');
      expect(chunk.endPosition, equals(chunkSize - 1), reason: 'endPosition');
      expect(chunk.bytesDownloaded, chunkSize, reason: 'bytesDownloaded');
      expect(chunk.isDownloading, isFalse, reason: 'isDownloading');
      expect(chunk.isCompleted, isTrue, reason: 'isCompleted');
    });
  });
  
  group('start()', () {
    group('success', () {
      var dataRange = new Range(0, TestServerSettings.contiguousChunkSize - 1);
      var expectedData = TestServerSettings.downloadFileData.getRange(dataRange.start, dataRange.end);
      var chunk = new DownloadChunk(okUrl, 0, 0, dataRange.length);
      var progressEvents = new List<DownloadProgressEvent>();
      
      test('no error is thrown', () {
        var future = chunk.start().listen((DownloadProgressEvent event) {
          progressEvents.add(event);
        })
        .asFuture()
        .catchError((error) {
          fail(error.message);
        });

        return future;
      });

      test('returns one or more progress events', () {
        var progEvents = progressEvents.where((event) {
          return event.name == DownloadProgressEvent.progressTypeName;
        });
        expect(progEvents.length, greaterThan(0));
      });

      test('returns no error events', () {
        var errorEvents = progressEvents.where((event) {
          return event.name == DownloadProgressEvent.errorTypeName;
        });
        expect(errorEvents.length, isZero);
      });
      
      test('returns a single chunk completion event', () {
        var completionEvents = progressEvents.where((event) {
          return event.name == DownloadProgressEvent.chunkCompletedTypeName;
        });
        expect(completionEvents.length, equals(1));
      });
      
      test('downloaded bytes are the same as the requested source byte range', () {
        var completionEvent = progressEvents.firstWhere((event) {
          return event.name == DownloadProgressEvent.chunkCompletedTypeName;
        });
        expect(completionEvent.data.asInt8List(), equals(expectedData));
      });
      
      test('downloaded bytes is the same as chunk size on completion', () {
        
      });
    });
    
    group('failure', () {
      
    });
  });
  
  group('re-start()', () {
      group('success', () {
        
      });
      
      group('failure', () {
        
      });
  });
  
  group('stop()', () {
    group('isRunning', () {
      
    });
    
    group('is already stopped', () {
      
    });
    
    group('never started', () {
      
    });
  });
}
