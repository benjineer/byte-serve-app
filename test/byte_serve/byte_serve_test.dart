part of byte_serve.tests;

byteServeTests() {
  
  var initialisedDownloadId = Download.generateId();  
  var connectionLimit = 4;
  var _byteServe = new ByteServe(chunkSize: TestServerSettings.contiguousChunkSize, connectionLimit: connectionLimit, serveOnCompletion: false);  
  StreamSubscription<DownloadProgressEvent> _streamSubscription;
  
  group('constructor', () {    
    test('chunkSize is set', () {      
      expect(_byteServe.chunkSize, equals(TestServerSettings.contiguousChunkSize));
    });
    
    test('connectionLimit is set', () {
      expect(_byteServe.connectionLimit, equals(connectionLimit));
    });
    
    test('cache is set', () {
      expect(_byteServe.cache, isNotNull);
    });
    
    test('register is set', () {
      expect(_byteServe.register, isNotNull);
    });
    
    test('streamController is set', () {
      expect(_byteServe.progressEventStream, isNotNull);
    });
    
    test('downloads is set', () {
      expect(_byteServe.downloads, isNotNull);
    });
    
    test('serveOnCompletion is set', () {
      expect(_byteServe.serveOnCompletion, isFalse);
    });
  });
  
  group('initialise()', () {
    test('no error is thrown', () {
      var completer = new Completer();      
      var register = new FileSystemRegister();
      
      register.initialise()
      .then((_) {
        register.clear()
        
        .then((_) {
          _byteServe.initialise()
          
            .then((_) {
              completer.complete(null);
            })
            .catchError((error) {
              fail(error.message);
              completer.complete(null);
            }
          );          
        });
      });      
      
      return completer.future;
    });
    
    test('initialises cache', () {
      expect(_byteServe.cache, isNotNull);
    });

    test('initialises register', () {
      expect(_byteServe.register, isNotNull);
    });

    test('reads all downloads from register and cache', () {
      var completer = new Completer();
      
      var download1 = initialisedDownload();
      var download2 = completedDownload();
      
      var cacheRecord1 = new FileSystemRecord.fromDownload(download1, null);
      var cacheRecord2 = new FileSystemRecord.fromDownload(download2, null);
      
      var originalPair1 = new DownloadCachePair(download1, cacheRecord1);
      var originalPair2 = new DownloadCachePair(download2, cacheRecord2);
      
      var byteServe = new ByteServe();
      var register = new FileSystemRegister();
      
      register.initialise()
      .then((_) {
        
        register.saveDownloads([originalPair1, originalPair2])
        .then((_) {
          
          byteServe.initialise()
          .then((_) {            
            var pair1 = byteServe.downloads[download1.id];
            var pair2 = byteServe.downloads[download2.id];
            
            if (pair1 == null || pair2 == null) {
              fail("downloads not found");
            }
            
            expect(pair1.download.id, equals(download1.id));
            expect(pair2.download.id, equals(download2.id));

            expect(pair1.cacheRecord.name, equals(download1.id));
            expect(pair2.cacheRecord.name, equals(download2.id));
            
            completer.complete(null);
          });
        });
      });
      
      return completer.future;
    });
  });
  
  group('addDownload()', () {
    var download = initialisedDownload(initialisedDownloadId);
    var progressEvents = new List<ByteServeEvent>();
    
    test('throws no errors', () {
      var completer = new Completer();
      
      _byteServe.addDownload(download)
      .then((addedDownload) {
      
        _streamSubscription = _byteServe.progressEventStream
        .listen((var event) {
            progressEvents.add(event);
            
            if (progressEvents.any((e) => e.name == CacheWriteEvent.wholeFileWrittenTypeName) &&
                progressEvents.any((e) => e.name == DownloadProgressEvent.downloadCompletedTypeName)) {
              new Timer(new Duration(milliseconds: 500), () { // wait to see if there are any more completion events
                
                if (!completer.isCompleted) {
                  completer.complete(null);
                }
              });
            }
          },
          
          onDone: () {
            completer.complete(null);
          },
          
          onError: (error) {
            fail(error.message);
            completer.complete(null);
          }
        );
      })
      .catchError((error) {
        fail(error.message);
        completer.complete(null);
      });
      
      return completer.future;
    });
    
    test('returns at least 1 progress event for each chunk', () {
      var progEvents = progressEvents.where((event) {
        return event.name == DownloadProgressEvent.progressTypeName;
      });
      
      expect(progEvents.length, greaterThanOrEqualTo(download.chunks.length));
    });
    
    test('returns no error events', () {
      var errorEvents = progressEvents.where((event) {
        return event.name == DownloadProgressEvent.errorTypeName;
      });
      
      expect(errorEvents.length, isZero);
    });
    
    test('returns a chunk completion event for each chunk', () {
      var chunkCompleteEvents = progressEvents.where((event) {
        return event.name == DownloadProgressEvent.chunkCompletedTypeName;
      });
      
      expect(chunkCompleteEvents.length, equals(download.chunks.length));
    });
    
    test('returns a single download completion event', () {
      var completionEvents = progressEvents.where((event) {
        return event.name == DownloadProgressEvent.downloadCompletedTypeName;
      });
      
      expect(completionEvents.length, equals(1));
    });
    
    test('progressEventStream returns a single part written event per chunk', () {
      var partWrittenCount = progressEvents.where((e) => e.name == CacheWriteEvent.partFileWrittenTypeName).length;
      var chunkCount = download.chunks.length;
      expect(partWrittenCount, equals(chunkCount));
    });
    
    test('progressEventStream returns a single whole file written event', () {
      var wholeFileWrittenCount = progressEvents.where((e) => e.name == CacheWriteEvent.wholeFileWrittenTypeName).length;
      expect(wholeFileWrittenCount, equals(1));
    });
      
    test('downloaded file is not corrupted', () {
      var downloadedData = [];
      var originalData = TestServerSettings.downloadFileData.toList(growable: false);
      var chunkCompleteEvents = progressEvents.where((e) => e.name = DownloadProgressEvent.chunkCompletedTypeName);
      
      for (DownloadProgressEvent event in chunkCompleteEvents) {
        var chunkData = event.data.asInt8List().toList(growable: false);
        downloadedData.addAll(chunkData);
      }
      
      expect(downloadedData, equals(originalData));
    });
  });

  group('getDownload()', () {
    test('returns the correct download item', () {
      expect(_byteServe.getDownload(initialisedDownloadId).id, equals(initialisedDownloadId));
    });
  });
  
  var pausedId = Download.generateId();
  var pausedDownload = unInitialisedDownload(pausedId);
  var completedChunkSizeBeforePause;
  var completedChunkSizeAfterPause;

  
  group('pause()', () {
    test('stops any more events coming through progressEventStream', () {
      var completer = new Completer();
      var pauseCalled = false;
      
      _byteServe.addDownload(pausedDownload)
      .then((_) {
        
        _streamSubscription.onData((event) {
          if (event.name == DownloadProgressEvent.chunkCompletedTypeName) {
            
            if (!pauseCalled) {
              
              if (pausedDownload.completeChunkCount == 2) {
                completedChunkSizeBeforePause = pausedDownload.getCompletedChunkSize();
                _byteServe.pause(pausedId);
                completedChunkSizeAfterPause = pausedDownload.getCompletedChunkSize();
                
                new Timer(new Duration(milliseconds: 500), () {
                  if (!completer.isCompleted) {
                    completer.complete(null);
                  }
                });
              }
            }
            else {
              fail('event received after pause()');
              completer.complete(null);
            }
          }
        });
      });
      
      return completer.future;
    });
    
    test('does not change the completed chunk size', () {
      expect(completedChunkSizeBeforePause, equals(2 * pausedDownload.chunkSize));
      expect(completedChunkSizeAfterPause, equals(completedChunkSizeBeforePause));
    });
  });

  
  group('resume()', () {
    var resumedEvents = new List<ByteServeEvent>();
    var completedChunkSizeAfterResume;
    
    test('clears all incomplete chunks', () {
      var completer = new Completer();
      
      _streamSubscription.onData((event) {
        resumedEvents.add(event);
        
        if (resumedEvents.length == 1) {
          expect(event.download.completeChunkCount, equals(2));
        }
        
        else if (event.name == DownloadProgressEvent.downloadCompletedTypeName) {
          new Timer(new Duration(milliseconds: 500), () {
            if (!completer.isCompleted) {
              completer.complete(null);
            }
          });
        }
      });

      _byteServe.resume(pausedId);
      completedChunkSizeAfterResume = pausedDownload.getCompletedChunkSize();
      return completer.future;
    });
    
    test('does not reduce the completed chunk size', () {
      expect(completedChunkSizeAfterResume, greaterThanOrEqualTo(completedChunkSizeBeforePause));
    });
    
    test('completes download', () {
      var resumedChunks = pausedDownload.chunks.length - 2;
      var progressEvents = resumedEvents.where((event) => event.name == DownloadProgressEvent.progressTypeName);
      var chunkCompletionEvents = resumedEvents.where((event) => event.name == DownloadProgressEvent.chunkCompletedTypeName);
      var downloadCompletionEvents = resumedEvents.where((event) => event.name == DownloadProgressEvent.downloadCompletedTypeName);
      var errorEvents = resumedEvents.where((event) => event.name == DownloadProgressEvent.errorTypeName);
      
      expect(progressEvents.length, greaterThan(resumedChunks));
      expect(chunkCompletionEvents.length, equals(resumedChunks));
      expect(downloadCompletionEvents.length, equals(1));
      expect(errorEvents.length, isZero);
    });
  });

  group('retry()', () {
    var retriedEvents = new List<ByteServeEvent>();

    test('clears all chunks', () {
      var completer = new Completer();
      
      _streamSubscription.onData((event) {
        if (event.downloadId == pausedId) {
          retriedEvents.add(event);
          
          if (retriedEvents.any((e) => e.name == CacheWriteEvent.wholeFileWrittenTypeName) &&
              retriedEvents.any((e) => e.name == DownloadProgressEvent.downloadCompletedTypeName)) {
            new Timer(new Duration(milliseconds: 500), () {
              if (!completer.isCompleted) {
                completer.complete(null);
              }
            });
          }
        }
      });
      
      _byteServe.retry(pausedId);
      expect(pausedDownload.getCompletedChunkSize(), isZero);
      expect(pausedDownload.getDownloadedPercentage(), lessThan(10));
      
      return completer.future;
    });
    
    test('progressEventStream returns more than 1 progress event per chunk', () {
      var progressCount = retriedEvents.where((e) => e.name == DownloadProgressEvent.progressTypeName).length;
      var chunkCount = pausedDownload.chunks.length;
      expect(progressCount, greaterThanOrEqualTo(chunkCount));
    });

    test('progressEventStream returns a single chunk completion event per chunk', () {
      var chunkCompleteCount = retriedEvents.where((e) => e.name == DownloadProgressEvent.chunkCompletedTypeName).length;
      var chunkCount = pausedDownload.chunks.length;
      expect(chunkCompleteCount, equals(chunkCount));
    });

    test('progressEventStream returns a single download completion event', () {
      var downloadCompleteCount = retriedEvents.where((e) => e.name == DownloadProgressEvent.downloadCompletedTypeName).length;
      expect(downloadCompleteCount, equals(1));
    });

    test('progressEventStream returns no error events', () {
      var errorCount = retriedEvents.where((e) => e.name == DownloadProgressEvent.errorTypeName).length;
      expect(errorCount, isZero);
    });
    
    test('progressEventStream returns a single part written event per chunk', () {
      var partWrittenCount = retriedEvents.where((e) => e.name == CacheWriteEvent.partFileWrittenTypeName).length;
      var chunkCount = pausedDownload.chunks.length;
      expect(partWrittenCount, equals(chunkCount));
    });
    
    test('progressEventStream returns a single whole file written event', () {
      var wholeFileWrittenCount = retriedEvents.where((e) => e.name == CacheWriteEvent.wholeFileWrittenTypeName).length;
      expect(wholeFileWrittenCount, equals(1));
    });
  });

  group('remove()', () {
    test('no more events are received', () {
      var completer = new Completer();
      
      _streamSubscription.onData((event) {
        if (event.downloadId == pausedId) {
          fail(event.name + " event received");
        }
      });
      
      _byteServe.remove(pausedId);
      
      new Timer(new Duration(milliseconds: 500), () {
        completer.complete(null);
      });
      
      return completer.future;
    });
    
    test('getDownload() returns null', () {
      expect(_byteServe.getDownload(pausedId), isNull);
    });
    
    test('download is removed from cache', () {
      return _byteServe.cache.findRecord(pausedId)
      .then((file) {
        expect(file, isNull);
      });
    });
  });

  group('serveFile()', () {
    
  });

  group('close()', () {
    var downloadsBeforeClose = new List<Download>();
    
    test('stops all downloads', () {
      var completer = new Completer();
      var download = new Download(okUrl, 1024 * 100);
      
      _byteServe.addDownload(download)
      .then((dl) {
        downloadsBeforeClose = _byteServe.downloads.values.map((pair) => pair.download);
        
        _byteServe.close()
        .then((_) {
          var activeDownloads = _byteServe.downloads.values.map((pair) => pair.download).where((dl) => dl.isDownloading);
          expect(activeDownloads.length, isZero);
          completer.complete(null);
        });
      });
      
      return completer.future;
    });
    
    test('saves all downloads to cache', () {
      var completer = new Completer();      
      _byteServe = new ByteServe(chunkSize: 1024 * 100, connectionLimit: 4, serveOnCompletion: false);
      
      _byteServe.initialise()
      .then((_) {
        expect(_byteServe.downloads.length, equals(downloadsBeforeClose.length));
        
        for (var download in downloadsBeforeClose) {
          expect(_byteServe.downloads.containsKey(download.id), isTrue);
        }
        completer.complete(null);
      });
      
      return completer.future;
    });
  });
}
