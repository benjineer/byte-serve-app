part of byte_serve.tests;

List <DownloadChunk> nonCompleteChunks() {
  var chunks = [];  
  var chunkCount = TestServerSettings.downloadFileData.length ~/ 10;
  var currentRange = new Range(0, TestServerSettings.contiguousChunkSize - 1);
  
  for (var index = 0; index < chunkCount; ++index) {
    chunks.add(new DownloadChunk(okUrl, index, currentRange.start, currentRange.end));    
    currentRange.start += TestServerSettings.contiguousChunkSize;
    currentRange.end += TestServerSettings.contiguousChunkSize;
  }
  
  return chunks;
}

List <DownloadChunk> completedChunks() {
  var chunks = [];  
  var chunkCount = TestServerSettings.downloadFileData.length ~/ 10;
  var currentRange = new Range(0, TestServerSettings.contiguousChunkSize - 1);
  
  for (var index = 0; index < chunkCount; ++index) {
    chunks.add(new DownloadChunk(okUrl, index, currentRange.start, currentRange.end, bytesDownloaded: TestServerSettings.contiguousChunkSize));    
    currentRange.start += TestServerSettings.contiguousChunkSize;
    currentRange.end += TestServerSettings.contiguousChunkSize;
  }
  
  return chunks;
}