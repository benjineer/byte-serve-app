part of byte_serve.tests;

final int completedDownloadChunkSize = TestServerSettings.contiguousChunkSize;
const int downloadConnectionLimit = 4;

Download completedDownload([String downloadId]) {
  var chunks = completedChunks();
  
  return new Download(okUrl, completedDownloadChunkSize,
    id: downloadId == null ? Download.generateId() : downloadId,
    size: chunks.length * TestServerSettings.contiguousChunkSize,
    mime: TestServerSettings.downloadFileMime,
    chunks: chunks,
    connectionLimit: downloadConnectionLimit,
    filename: TestServerSettings.downloadFileName,
    completeChunkCount: chunks.length
  );
}

Download unInitialisedDownload([String downloadId]) {
  return new Download(okUrl, completedDownloadChunkSize,
    id: downloadId == null ? Download.generateId() : downloadId
  );
}

Download initialisedDownload([String downloadId]) {
  var chunks = nonCompleteChunks();
    
  return new Download(okUrl, completedDownloadChunkSize,
    id: downloadId == null ? Download.generateId() : downloadId,
    size: chunks.length * TestServerSettings.contiguousChunkSize,
    mime: TestServerSettings.downloadFileMime,
    chunks: chunks,
    connectionLimit: downloadConnectionLimit,
    filename: TestServerSettings.downloadFileName,
    completeChunkCount: 0
  );
}